export const up = (knex, Promise) => {
    return knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto"')
                .createTable('books', (table) => {
                table.bigIncrements('id').primary(); // big serial
                table.bigInteger('created_at').notNullable();
                table.bigInteger('updated_at').notNullable();
                table.boolean('deleted').notNullable();
                table.text('title').notNullable();
                table.text('author').notNullable();
                table.jsonb('details');
                });
  };
  
export const down = (knex, Promise) => {
    return knex.schema.dropTableIfExists('books');
};