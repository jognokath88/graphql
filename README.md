# Working output

[![](./media/graphql-docker.png)](https://youtu.be/csLni6sNyiE "Working Output - postgres docker project by kate")


<iframe width="560" height="315" src="https://youtu.be/csLni6sNyiE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<figure class="video_container">
  <iframe src="./media/graphql-docker.mkv" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>
