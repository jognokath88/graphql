FROM node:18-alpine
WORKDIR /app
COPY . .
EXPOSE 4001
CMD npm run start