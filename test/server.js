// index.js

import Koa from 'koa';
import bodyParser from "koa-bodyparser";
import cors from "@koa/cors";
import { ApolloServer } from "@apollo/server";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { koaMiddleware } from "@as-integrations/koa";
import http from 'http';

import resolvers from '../src/resolvers/index.js';
import typeDefs from '../src/types/index.js';

const app = new Koa()
const httpServer = http.createServer(app.callback());

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context : ({ctx}) => ctx,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
})

await server.start();
 
app
    .use(cors())
    .use(bodyParser())
    .use(
        koaMiddleware(server, {
            context: async ({ ctx }) => ({ token: ctx.headers.token }),
        })
      );


await new Promise((resolve) => httpServer.listen({ port: 4001}, resolve));
console.log(`Server up at http://localhost:4001/`);