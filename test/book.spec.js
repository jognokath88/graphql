import chai, {expect} from 'chai';
import { describe } from 'node:test';
import supertest from 'supertest';

const url = 'http://localhost:4001';
const request = supertest(url);

describe('GraphQL', () => {
    it('Query: Retrieve all books', async function() {

        const query = `query Query {
            books {
              id
              author
              title
            }
          }`;
        const res = await request.post('/graphql').send({  query: query  });
            
        expect(res.body.data.books).to.not.undefined;
        
    })

    it('Query: Retrieve a single book with valid ID', async function() {
        const data = { bookId : "5" };
        const query = `query Query($bookId: ID!) {
            book(id: $bookId) {
                author
                title
            }
        }`;
        const res = await request.post('/graphql').send({
            query: query,
            variables : data,
        });

        expect(res.body.data.book).to.not.undefined;
             
    })
            
    it('Query: Retrieve a single book with invalid ID', async function() {
        const data = { bookId : "xxx" };
        const query = `query Query($bookId: ID!) {
            book(id: $bookId) {
                author
                title
            }
        }`;
        const res = await request.post('/graphql').send({
            query: query,
            variables : data,
        });
          
        expect(res.body.data.book).to.null;
        
    })

    it('Mutation: Update a book with valid ID, supply all fields (e.g. title, author, details)', async function() {
        const data = {
            "updateBookId": "9",
            "book": {
              "author": "JK Rowling",
              "details": "{\"isbn\" : \"0-1884-3930-7\",  \"publisher\" : \"Bloomsbury, United Kingdom \",      \"publication_year\": 1998}",
              "title": "Harry Potter and the Chamber of secrets2"
            }
          };
        
        const mutation = `mutation UpdateBook($updateBookId: ID!, $book: CreateBookInput) {
            updateBook(id: $updateBookId, book: $book) {
              id
            }
          }`;
        const res = await request.post('/graphql').send({
            query: mutation,
            variables: data
        });
            
        expect(res.body.data.updateBook.id).to.not.undefined;   
        
    })

    it('Mutation: Update a book with invalid ID, all required fields supplied (e.g. title, author)', async function() {
        const data = {
            "updateBookId": "0",
            "book": {
              "author": "Edgar allan poe",
              "title": "The Raven"
            }
          };
        
        const mutation = `mutation UpdateBook($updateBookId: ID!, $book: CreateBookInput) {
            updateBook(id: $updateBookId, book: $book) {
              id
            }
          }`;
        
        const res = await request.post('/graphql') .send({
            query: mutation,
            variables: data
        });

        expect(res.body.data.updateBook.id).to.null;
        
    })

    it('Mutation: Create a book, supply all fields (e.g. title, author, details)', async function() {
        const data = {
            title: 'Harry Potter and the philophers stone',
            author: 'JK Rowling',
            details: "{\"isbn\" : \"0-4879-5951-5\",  \"publisher\" : \"Bloomsbury, United Kingdom \",      \"publication_year\": 1996}"
        };

        const mutation = `mutation CreateBook($data: CreateBookInput) {
            createBook(book: $data) {
              ... on CreateBookSuccess {
                id
              }
              ... on CreateBookError {
                message
              }
            }
          }`;
        const res = await request.post('/graphql').send({
            query: mutation,
            variables: {data}
        });         
        
        expect(res.body.data.createBook.id).to.not.undefined;
        
    })

    it('Mutation: Create a book, some required fields not supplied (e.g. title)', async function() {

        const data = {
            "book": {
              "author": "Regina Vanguardia"
            }
          };
        
        const mutation = `mutation CreateBook($book: CreateBookInput) {
            createBook(book: $book) {
              ... on CreateBookSuccess {
                id
              }
              ... on CreateBookError {
                message
              }
            }
          }`;
        
        const res = await request.post('/graphql').send({
            query: mutation,
            variables: data
        });
            
            
        expect(res.body.errors).to.not.undefined;
        
    })

   
    it('Mutation: Soft delete a book given a valid id', async function() {
        const data = { deleteBookId: "21" };
        const mutation = `mutation DeleteBook($deleteBookId: ID!) {
            deleteBook(id: $deleteBookId) {
              id
            }
          }`;

        const res = await request.post('/graphql') .send({
            query : mutation,
            variables: data
        });
           
        expect(res.body.data.deleteBook).to.not.undefined;
        
    })

    it('Mutation: Soft delete a book given an invalid id', async function() {
        const deleteBookId = { deleteBookId: "0" };
        const mutation = `mutation DeleteBook($deleteBookId: ID!) {
            deleteBook(id: $deleteBookId) {
              id
            }
          }`;

        const res = await request.post('/graphql').send({
            query : mutation,
            variables: deleteBookId
        });
            
        expect(res.body.data.deleteBook.id).to.null;
        
    })
})