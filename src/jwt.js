// jwt.js

import jwt from 'jsonwebtoken';
import {ApolloServerErrorCode } from "@apollo/server/errors";

// relay - far more advanced apollo
// apollo connects node

export function verifyToken(ctx) {
    const auth = ctx.request.header.authorization;

    if (auth === undefined) {
        return ;
    }

    const [prefix, raw] = auth.split(' ');

    if (prefix !== 'Bearer') {
        return ;

    }

    try {
        return jwt.verify(raw, 'secret');
    } catch (err) {
        throw new ApolloServerErrorCode.AuthenticationError('Unauthenticated');
    }
}


export function parseScopes(token) {
    return token.scopes;
}

