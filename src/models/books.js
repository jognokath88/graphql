// models/books.js

// where data is coming from

import QueryBuilder from "../querybuilder.js";

const qb = QueryBuilder();

export async function retrieveAll() {
    return await qb.select('books').returning('*');
}

export async function retrieve(id) {

    return await qb.select('books')
            .where({id: id});

}

export async function insert(books) {
    const now = Math.floor(Date.now() / 1000);

    const data = {
        ...books,
        updated_at: now,
        created_at: now,
        deleted: false
    };

    return await qb.insert('books', data).returning('*');
}

export async function update(books, id) {
    const now = Math.floor(Date.now() / 1000);

    const data = {
        ...books,
        updated_at: now,
    };

    const q = await qb.update('books', data, id).returning('*');
    const isUpdated = Object.entries(q).length > 0;
    
    return isUpdated;
}

export async function del(id) {
   const q = await qb.softDelete('books', id).returning('*');

   const isSoftDeleted = Object.entries(q).length > 0;
   
   return isSoftDeleted;
}

const proto = {
    retrieveAll,
    retrieve,
    insert,
    update,
    del
}

export default () => {
    return Object.assign({}, proto);
}

/*
function parseBooks(row) {
    return Object.assign({}, {
        id: row.id,
        created_at: parseInt(row.created_at),
        updated_at: parseInt(row.updated_at),
        deleted: row.deleted,
        title: row.title,
        author: row.author
    })
}

// create schema
export const schema = {
    create : [
        `CREATE TABLE books (
            id BIGSERIAL PRIMARY KEY,
            created_at BIGINT NOT NULL,
            updated_at BIGINT NOT NULL,
            deleted BOOLEAN NOT NULL,
            title TEXT NOT NULL,
            author TEXT NOT NULL
        )`
    ],

    truncate : [
        `TRUNCATE TABLE books`
    ],

    drop : [
        `DROP TABLE IF EXISTS books`
    ]
}

// get
export async function retrieveAll () {
    const q = qb.select('books')
        .clone()
        .toParam()

    return (await pg.rowsArgs(q.text, q.values)).map(parseBooks);


}

// individual get
export async function retrieve(pg, id) {
    const q = qb.select('books')
                .clone()
                .where('id = ?',id)
                .toParam()
    
    const rows = await pg.rowsArgs(q.text, q.values);

    if (rows.length === 0 ) return

    return parseBooks(rows[0])
}

// post
export async function insert(pg, books) {

    const now = Math.floor(Date.now() / 1000)

    const data = {
        ...books,
        updated_at: now,
        created_at: now,
        deleted: false
    }

    const q = qb.insert('books', data)
        .clone()
        .returning('id')
        .toParam()
    
    return await pg.valueArgs(q.text, q.values)
}

// update
export async function update (pg, books, id) {
    const {author, title} = books 

    const q = qb.update('books', books, id)
            .clone()
            .returning('id')
            .set('title = ?', title)
            .set('author = ?', author)
            .toParam()

    const rows = await pg.queryArgs(q.text, q.values)
    return rows.rowCount !== 0
}

export async function del(pg, id) {
    const q = qb.softDelete('books', id)
        .clone()
        .returning('id')
        .toParam()

    const rows = await pg.queryArgs(q.text, q.values)

    return rows.rowCount !== 0
}*/
