// resolvers/books.js - graphql

import Books from '../models/books.js'
// import { postgres } from '../postgres.js'

// use multiple databases in graphql

const Book = Books()

export default {
    // query are for getting data
    Query : {
        books : async(_, __, ctx) => {
            const books = await Book.retrieveAll();
            return books;
        },

        book : async (_, {id}, ctx) => {
            const book = await Book.retrieve(id);
            return book[0];
        }
    },

    // mutation is for changing data
    // post put patch delete
    Mutation : {
        createBook: async(_, {book}, ctx) => {
            const book_ = await Book.insert(book);
            const id = book_[0]['id'];
            return {id};
        },

        updateBook : async(_, {id, book}, ctx) => {
            const isUpdated = await Book.update(book, id);

            return isUpdated ? {id} : {};
        },

        deleteBook : async (_, {id}, ctx) => {
            const isdeleted = await Book.del(id);

            return isdeleted ? {id} : {};
        }
    },

    CreateBookPayload : {
        __resolveType(obj) {
            if(obj.id) {
                return 'CreateBookSuccess';
            }

            if (obj.message) {
                return 'CreateBookError';
            }
            return null;
        }
    }

}