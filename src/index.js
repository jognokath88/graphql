// index.js

import Koa from 'koa';
import bodyParser from "koa-bodyparser";
import cors from "@koa/cors";
import logger from 'koa-logger';
import { ApolloServer } from "@apollo/server";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { koaMiddleware } from "@as-integrations/koa";
import http from 'http';

import resolvers from './resolvers/index.js';
import typeDefs from './types/index.js';

import AuthDirective from './directives/auth.js';

const app = new Koa();
const httpServer = http.createServer(app.callback());

const server = new ApolloServer({
    typeDefs,
    resolvers,
    schemaDirectives : {
        auth : AuthDirective
    },
    context : ({ctx}) => ctx,
    playground : {
        settings : {
            'editor.cursorShape': 'line'
        }
    },
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
});

await server.start();
 
app
    .use(cors())
    .use(logger())
    .use(bodyParser())
    .use(
        koaMiddleware(server, {
            context: async ({ ctx }) => ({ token: ctx.headers.token }),
        })
      );


await new Promise((resolve) => httpServer.listen({ port: 4000 }, resolve));
console.log(`Server up at http://localhost:4000/`);