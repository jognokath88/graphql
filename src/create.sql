-- create.sql

SELECT 'CREATE DATABASE academy3' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'academy3');
\connect academy3;
CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE TABLE books (
    id BIGSERIAL PRIMARY KEY,
    created_at BIGINT NOT NULL,
    updated_at BIGINT NOT NULL,
    deleted BOOLEAN NOT NULL,
    title TEXT NOT NULL,
    author TEXT NOT NULL,
    details JSONB
);