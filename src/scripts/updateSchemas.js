// scripts/updateSchemas
// translation process
// import PgAsync from 'pg-async';
import knex from '../postgres.js';
// import dotenv from 'dotenv-safe';
// import pgm from 'node-pg-migrate';

// dotenv.load()

const ignoredSchemas = [
    'public', 'extensions', 'pg_catalog', 'pg_toast', 'pg_temp1', 'pg_toast_temp1',
    'information_schema']

// const dbUri = process.ennv['DB_URI']

async function updateSchemas() {
    // const pg = new PgAsync( {conectionString: dbUri})
    const rows = await knex('information_schema.schemata').select('schema_name'); 

    const schemas = rows.map(row => row.schema_name);

    const filtered = schemas.filter(schema => !ignoredSchemas.includes(schema));
    // console.log(filtered)
    for (let i = 0; i < filtered.length; i++) {
        console.log(`** MIGRATING ${filtered[i]} **`);

        // await knex.migrate.up();

        console.log(`** FINISHED ${filtered[i]} **`);
    }
    // pg.closeConnections()
}

updateSchemas().then(()=> console.log('DONE')).catch((err) => console.error(err));