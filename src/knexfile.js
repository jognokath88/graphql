import path from 'node:path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const BASE_PATH = path.join(__dirname);

export default {
  development: {
    client: 'pg',
    connection: 'postgres://postgres@postgres:5432/academy3',
    migrations: {
      directory: path.join('../', 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH,'seeds')
    }
  }
};