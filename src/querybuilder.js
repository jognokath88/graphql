// querybuilder.js
import knex from './postgres.js';
import fs from 'fs';
import path from 'path';
import url  from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const pubKey = fs.readFileSync(`${__dirname}/public.key`, 'utf-8').toString();
const secretKey = fs.readFileSync(`${__dirname}/secret.key`,'utf-8').toString();

export function select(table) {
    return knex(table)
            .select(
                'id',
                'created_at',
                'updated_at',
                'deleted',
                knex.raw(`pgp_pub_decrypt(title::bytea, dearmor('${secretKey}'),'kwekkwek88') as title`),
                'author',
                'details'
            ).
            where(knex.raw(`NOT ${table}.deleted`));
}

export function insert(table, books) {
    // details column  is nullable
    // if supplied do this
    if (books.details) {
        return knex(table)
        .insert({
            created_at: books.created_at,
            updated_at: books.updated_at,
            deleted: books.deleted,
            title: knex.raw(`pgp_pub_encrypt('${books.title}', dearmor('${pubKey}'))`),
            author: books.author,
            details: books.details
        });
    }
    // if details not supplied do this
    return knex(table)
        .insert({
            created_at: books.created_at,
            updated_at: books.updated_at,
            deleted: books.deleted,
            title: knex.raw(`pgp_pub_encrypt('${books.title}', dearmor('${pubKey}'))`),
            author: books.author,
        });
   
}

export function update(table, books, id) {
    if (books.details) {
    return knex(table)
            .update({
                updated_at: books.updated_at,
                title: knex.raw(`pgp_pub_encrypt('${books.title}', dearmor('${pubKey}'))`),
                author: books.author,
                details: books.details
            })
            .where({ id: parseInt(id) })
    }

    return knex(table)
    .update({
        updated_at: books.updated_at,
        title: knex.raw(`pgp_pub_encrypt('${books.title}', dearmor('${pubKey}'))`),
        author: books.author,
    })
    .where({ id: parseInt(id) })


}

export function softDelete(table, id){
    return knex(table)
            .update({
                deleted: true,
            })
            .where({id : parseInt(id)})
            .where(knex.raw(`NOT ${table}.deleted`));
}
// merge in one object
const proto = {
    select,
    insert,
    update,
    softDelete
};

export default function() {
    return Object.assign({}, proto)
};
/*
// a part of knex
import squel from 'safe-squel'; 
const sq = squel.useFlavour('postgres');

export function select(table) {
    // javascript way of talking to sql
    return sq
            .select()
            .field('id')
            .field('created_at')
            .field('updated_at')
            .field('author')
            .field('deleted')
            .field('title')
            .from(table)
            .where(`NOT ${table}.deleted`)

}

export function insert(table, values) {
    return sq
            .insert()
            .into(table)
            .setFields(values)
}

export function update(table, data, id) {
    return sq
            .update()
            .table(table)
            .where('id = ?', id)
}

export function softDelete(table, id) {
    return sq
            .update()
            .table(table)
            .set('deleted', true)
            .where('id = ?', id)
            .where(`NOT ${table}.deleted`)
}*/
