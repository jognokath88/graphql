import fs from 'fs';
// create pgp public and private keys
const pubKey = fs.readFileSync('../public.key', 'utf-8').toString();
const secretKey = fs.readFileSync('../secret.key','utf-8').toString();

export const seed = (knex, Promise) => {
    return knex('books').del()
    .then(() => {
        return knex('books').insert({
            created_at: Math.floor(Date.now() / 1000),
            updated_at: Math.floor(Date.now() / 1000),
            deleted: false,
            title:  knex.raw(`pgp_pub_encrypt('The Hitchhikers Guide to the Galaxy', dearmor('${pubKey}'))`),
            author : "Douglas Adams",
            details : {
                publisher: "Pan Books, London",
                publication_year: 1985,
                isbn : "0-330-29288-9"
                }
            })
    })
    .then(() => {
      return knex('books').insert({
        created_at: Math.floor(Date.now() / 1000),
        updated_at: Math.floor(Date.now() / 1000),
        deleted: false,
        title:  knex.raw(`pgp_pub_encrypt('The Hunger Games', dearmor('${pubKey}'))`),
        author : 'Suzanne Collins',
        details : {
            publisher: 'Scholastic Press, United States',
            publication_year: 2008,
            isbn : '978-0-439-02352-8'
        }
      });
    })
    .then(() => {
      return knex('books').insert({
        created_at: Math.floor(Date.now() / 1000),
        updated_at: Math.floor(Date.now() / 1000),
        deleted: false,
        title:  knex.raw(`pgp_pub_encrypt('Dune', dearmor('${pubKey}'))`),
        author : 'Frank Herbert',
        details : {
            publisher: 'Chilton Books, United States',
            publication_year: 1965,
            isbn : '978-0441172719'
        }
      });
    });
  };
