import path from 'path';
import { mergeTypeDefs } from '@graphql-tools/merge';
import { loadFiles } from '@graphql-tools/load-files';
import url  from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const typesArray = await loadFiles(path.join(__dirname), {
    ignoreIndex: true,
    requireMethod: async path => {
      return await import(url.pathToFileURL(path));
    }
});

export default mergeTypeDefs(typesArray);

