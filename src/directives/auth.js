// directives/auth.js
import {defaultFieldResolver} from 'graphql';
import {SchemaDirectiveVisitor} from "@graphql-tools/utils";
import {ApolloServerErrorCode} from "@apollo/server/errors";
import { verifyToken, parseScopes } from '../jwt.js';

export default class AuthDirective extends SchemaDirectiveVisitor {
    visitObject(type) {
        console.log('TYPE', type);
        console.log('ARGS', this.args);
    }

    visitFieldDefinition(field, details) {
        console.log('TYPE', field);
        console.log('DETAILS', details);
        console.log('ARGS', this.args);

        const {resolve = defaultFieldResolver} = field;

        field.resolve = async (...args) => {
            const context = args[2];

            const token = verifyToken(context);

            if(!token) {
                throw new ApolloServerErrorCode.AuthenticationError('Unauthenticated');
            }

            const { scope } = this.args;
            const tokenScopes = parseScopes(token);

            if ( !tokenScopes || !tokenScopes.includes(scope) ) {
                throw new ApolloServerErrorCode.ForbiddenError('Unauthorized');
            }

            return resolve.apply(this.args);

        }
    }

}